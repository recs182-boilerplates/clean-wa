class cAside{

    constructor(Post){
        this.Post = Post;

        this.pages = [];

        this.init();
    }

    init(){
        this.Post('pages/select-all').then(res => {
            this.pages = resolveStructure(res, 'id_feature');
        }, err => {
            console.log('err', err);
        });
    }
}