function Post($http, $q, CONFIGS){
	const ext = urlExtension(CONFIGS.APPROVAL);
	return (url, data) => $q((resolve, reject) => {
		return $http.post(ext + 'api/' + url, data).then(res => {
			const result = res.data;
			result.hasOwnProperty('error') ? reject(result) : resolve(result);
		}, err => {
			reject(err.data);
		});
	});
}