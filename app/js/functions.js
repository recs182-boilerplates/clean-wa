function urlExtension(approval){
    const url        = location.href;
    const regexUrl   = /(http:\/\/|https:\/\/)(.*)/i;
    const regexLocal = /(192\.168\.1|127\.0\.0|localhost)/i;

    // PRODUCTION
    if(!url.match(regexUrl)) return '/';

    let [complete, prefix, parcial, index, input] = url.match(regexUrl);
    let matchLocal                                = parcial.match(regexLocal);

    // LOCAL DEVELOPMENT
    if(matchLocal){
        let splitted_parcial = parcial.split('/');
        let joins = [];
        for(let splits of splitted_parcial){
            joins.push(splits);
            if(splits.match(/dist/)) break;
        }
        return prefix + joins.join('/') + '/';
    }
    // PRODUCTION FALLBACK
    if(approval.active) return url;
    return '/';
}

function swap(arr, x, y){
    let b  = arr[x];
    arr[x] = arr[y];
    arr[y] = b;
    return arr;
}

function searchArray(arr, field, value){
    for(let index in arr){
        let val = arr[index];
        if(val[field] == value) return val;
    }
}

function searchArrayMultiple(arr, field, value){
    let result = [];
    for(let index in arr){
        let val = arr[index];
        if(val[field] == value) result.push(val);
    }
    return result;
}

function copy(o) {
    let output, v, key;
    output = Array.isArray(o) ? [] : {};
    for (key in o) {
        v = o[key];
        output[key] = (typeof v === "object") ? copy(v) : v;
    }
    return output;
}

function isLocalhost(url){
    const testRegex = /192\.168\.1|127\.0\.0|localhost/i;
    return url.match(testRegex) ? true : false;
}

function resolveStructure(arr, field){
    const structure = arr.filter(item => Number(item[field]) === 0);

    // sub level
    if(structure.length){
        structure.forEach(main => {
            main.items = searchArrayMultiple(arr, field, main.id);

            // last level
            if(main.items.length){
                main.items.forEach(item => {
                    item.items = searchArrayMultiple(arr, field, item.id);
                })
            }
        })
    }
    return structure;
}

//////////////////////////////////////////
// Pollyfills
//////////////////////////////////////////
if(!Array.prototype.includes) {
    Object.defineProperty(Array.prototype, 'includes', {
        value: function(searchElement, fromIndex) {
            
            // 1. Let O be ? ToObject(this value).
            if (this == null) {
                throw new TypeError('"this" is null or not defined');
            }
            
            var o = Object(this);
            
            // 2. Let len be ? ToLength(? Get(O, "length")).
            var len = o.length >>> 0;
            
            // 3. If len is 0, return false.
            if (len === 0) {
                return false;
            }
            
            // 4. Let n be ? ToInteger(fromIndex).
            //    (If fromIndex is undefined, this step produces the value 0.)
            var n = fromIndex | 0;
            
            // 5. If n ≥ 0, then
            //  a. Let k be n.
            // 6. Else n < 0,
            //  a. Let k be len + n.
            //  b. If k < 0, let k be 0.
            var k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);
            
            // 7. Repeat, while k < len
            while (k < len) {
                // a. Let elementK be the result of ? Get(O, ! ToString(k)).
                // b. If SameValueZero(searchElement, elementK) is true, return true.
                // c. Increase k by 1.
                // NOTE: === provides the correct "SameValueZero" comparison needed here.
                if (o[k] === searchElement) {
                    return true;
                }
                k++;
            }
            
            // 8. Return false
            return false;
        }
    });
}

if (!Object.values) {
    Object.values = function values(O) {
        return _reduce(_keys(O), (v, k) => _concat(v, typeof k === 'string' && _isEnumerable(O, k) ? [O[k]] : []), []);
    };
}

if (!Object.entries) {
    Object.entries = function entries(O) {
        return _reduce(_keys(O), (e, k) => _concat(e, typeof k === 'string' && _isEnumerable(O, k) ? [[k, O[k]]] : []), []);
    };
}