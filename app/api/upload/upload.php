<?php
require_once('_libraries/ThumbLib/ThumbLib.inc.php');

/**
 * @param $src
 * @param $destination
 * @param $width
 * @param $height
 */
function resize($src, $destination, $width, $height){
    $thumb = PhpThumbFactory::create($src , array('resizeUp' => true));
    $thumb->resize($width, $height)->backgroundFillWithColor($width, $height, 255);
    $thumb->save($destination);
}

$data = @$_POST['data'];
$folder = @$_POST['folder'];
$resize = @$_POST['resize'] || false;

isAdmLogged();

foreach($data as $file){
    $data = explode(',', $file['base64']);
    $imagePath = $folder . $file['name'];
    file_put_contents($imagePath, base64_decode($data[1]));

    if($resize){
        $sizes = array(75, 350, 800);
        foreach($sizes as $size){
            $destination = $folder . $size .'-'. $file['name'];
            resize($imagePath, $destination, $size, $size);
        }
        unlink($folder . $file['name']);
    }
}

return true;