<?
header('Content-Type: text/html; charset=utf-8');
header('Content-Type: application/json');

/*OBJECT*/
class object_ extends ArrayObject
{
	public $value;

	function __construct( $value = null )
	{
		$this->value = $value;
	}

	function __invoke( $value )
	{
		$this->value = $value;
		return $this;
	}

	function values()
	{
		return O( array_values($this->value) );
	}

	function keys()
	{
		return O( array_keys($this->value) );
	}

	function query()
	{
		return O( sqlQuery($this->value) );
	}

	function insert( $table )
	{
		return O( sqlInsert( $table, $this->value ) );
	}

	function grouping($id = "", $tables = [] )
	{
		if(count($this->value) == 0) return $this->value;
		$result        = [];
		$groups_fields = [];
		foreach($tables as $table)
		{
			foreach( array_keys( $this->value[0] ) as $k )	
			{
				if( strpos($k, $table) === 0 )
				{
					$groups_fields[$table][] = $k;
				}
			}
		}

		foreach( $this->value as $v )
		{
			$isset = isset( $result[ $v[$id] ]);
			if( !$isset )
				$result[ $v[$id] ] = $v;
			
			foreach($groups_fields as $table => $fields)
			{
				$group = [];
				if( !$isset )
					foreach($fields as $field)
					{
						$group[$field] = $v[$field];
						unset($result[ $v[$id] ][$field]);
					}
				else
					foreach($fields as $field)
					{
						$group[$field] = $v[$field];
					}	
				
				$result[ $v[$id] ][$table][] = $group;
			}
		}
		return O(array_values( $result ));
	}

	function escape()
	{
		return O( sqlEscape($this->value) );
	}

	function each( $fun )
	{
		foreach( $this->value as $v ) $fun($v);
		return $this;
	}

	function walk( $fun )
	{
		array_walk( $this->value, $fun );
		return $this;
	}

	function map( $fun )
	{
		return O( array_map( $fun, $this->value ) );
	}

	function filter( $fun = null )
	{
		if(!$fun){
			return O( array_filter( $this->value ) );
		}
		else
			return O( array_filter( $this->value, $fun ) );
	}

	function reduce( $fun, $initial = NULL )
	{
		return O( array_reduce($this->value, $fun, $initial) );
	}

	function split($sep = '')
	{
		return O( explode($sep, $this->value ) );
	}

	function join($sep = '')
	{
		return O( implode($sep, $this->value ) );
	}

	function print()
	{
		echo toJSON( $this->value );
		return $this;
	}

    function call($fun, ...$arguments)
    {	
		return $fun->bindTo( $this );
    }
    //$this[$index]
    function offsetGet($index)
    {
    	return $this->value[$index];
    }
    //$this[$index] = $value
    function offsetSet ( $index , $value )
    {
    	$index ?
    		$this->value[ $index ] = $value:
    		$this->value[]         = $value;
    }

    function get( $field, $or = '' )
    {
    	return isset($this->value[$field]) ? 
    		$this->value[$field] :
    		$or;
	}
	
	function getO( $field, $or = '' )
    {
    	return isset($this->value[$field]) ? 
    		O( $this->value[$field] ) :
    		O( $or );
    }

    function val()
    {
    	return $this->value;
    }

    function length()
    {
    	return count($this->value);
    }

    function __toString()
    {
    	return (string)$this->value;
    }

    function isset( $field )
    {
    	return isset($this->value[$field]);
    }
    function unset($field)
    {
    	unset($this->value[$field]);
    	return $this;
    }
}

function O( $value = null )
{
	return new object_( $value );
}

/*UTIL FUNCTIONS*/

function post( $url, $data = [] )
{
	$_POST_OLD = $_POST;
	$_POST     = is_object($data) ? $data : O($data);
	$result    = include( "$url.php" );
	$_POST     = $_POST_OLD;
	return $result;
}

function curl($method = "GET", $url, $data = [], $headers = [] )
{
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

	curl_setopt($ch,CURLOPT_URL, $url);
	if($method == "POST")
	{
		curl_setopt($ch,CURLOPT_POST, 1);
		curl_setopt($ch,CURLOPT_POSTFIELDS, http_build_query($data) );
	}
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers );
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	$result = curl_exec($ch);//to JSON???
	$err    = curl_error($ch);
	if($err) return $err;
	return $result;
}

function error( $msg ){
    echo json_encode( ["error" => $msg ] );
    http_response_code(400);
    die();    
}

function toJSON( $value, $options = null )
{
	return json_encode($value, $options);
}

function fromJSON( $value )
{
	return json_decode( $value, true );
}

function isAssoc($arr)
{
	return array_keys($arr) !== range(0, count($arr) - 1);
}

class compose_
{
	public $funs;
	function __construct( $funs = [] )
	{		
		$this->funs = $funs;
	}

	function __invoke( ...$args )
	{
		$i = 0;
		$fun  = $this->funs[$i];
		while( $fun )
		{
			$args = [call_user_func_array($fun, $args )];
			$fun  = $this->funs[++$i];
		}
		return @$args[0] ?? null;
	}
}

function compose( ...$args )
{
	return new compose_($args);
}

function session( $var )
{
	if( isset( $_SESSION[$var] ) )
		return $_SESSION[$var];
	else
		return null;
}

/*MYSQL*/
$__connection = null;

function sqlConnect( $address, $user, $pass, $db){
	global $__connection;
	$__connection = new mysqli($address, $user, $pass, $db);
	if ($__connection->connect_errno)
	{
		error( 'Failed to connect to MySQL: (' . $mysqli->connect_errno . ") " . $mysqli->connect_error  );
		die();
	}
	$__connection->set_charset("utf8");
}

function sqlEscape($str){
	global $__connection;
	return $__connection->real_escape_string($str);
}

function sqlSelectDb($dbname){
	global $__connection;
	mysqli_select_db($__connection,$dbname) or error("N�o foi poss�vel encontar o banco");		
}

function sqlQuery($query, $stop_onError = true){
	global $__connection;
	$results = $__connection->query($query);
    $res = array();
    if($results)
    {
		if(is_object($results))
		{
			while($row = $results->fetch_assoc()) $res[]=$row;
		}
    }else{
    	if($stop_onError)
    		error( 'Mysql Error: ' . $__connection->error . PHP_EOL. $query );
    }
    if( strpos($query, "insert into") === 0 ) return $__connection->insert_id;
    return $res;
}
function sqlInsert( $table, $data, $show = false ){
	global $__connection;

	$primary = sqlQuery("SHOW KEYS FROM $table WHERE Key_name = 'PRIMARY'");
	$primary = $primary ? $primary[0]['Column_name'] : '';

	$return_array = true;
	if( isAssoc( $data ) )
	{
		$data = [$data];
		$return_array = false;
	}



	$fields       = array_keys( $data[0] ) ;
	$fields_no_id = array_filter($fields, function($e)use($primary){ return !($e == $primary); } );

	$values = [];
	$return_ids = [];
	foreach( $data as $k => $v )
	{
		$values[]     = '\''. implode('\',\'', array_values( $v ) ).'\'';
		$return_ids[] = @$v[$primary] ?? null;
	}
	$values = '('.implode('),(', $values).')';

	$key_values = implode(',', array_map(function($e){ return $e . "= values($e)"; }, $fields_no_id) );

	$fields = implode(',', $fields);
	$query = 'insert into '.$table.' ('.$fields.') values '.$values.' on duplicate key update '.$key_values;

	
	$return_id = sqlQuery( $query );
	foreach($return_ids as &$rid){
		if( ( $rid == null) ||
			( is_int($rid) && ($rid == 0)) ||
			( is_object($rid) && ($rid->val() == 0) ))
		{
			$rid = $return_id++;
		}
	}

	return $return_array ? $return_ids : $return_ids[0] ;
}

function left_join($data){ return 'left join '.$data['from'].' on '.$data['on']; };
function right_join($data){ return 'right join '.$data['from'].' on '.$data['on']; };
class SqlBuild_{
	private $selects 	= array();
	private $froms		= array();
	private $left_join	= array();
	private $right_join	= array();
	private $wheres    	= array();
	private $whereors  	= array();
	private $limits     = array();
	private $orders    	= array();
	private $groups    	= array();
	private $having    	= array();

	function select( $str ){
		$this->selects[] = $str;
		return $this;
	}
	function from($str){
		$this->froms[] = $str;
		return $this;
	}
	function leftJoin($from, $on){
		$this->left_join[] = array( 'from'=> $from, 'on'=>$on );
		return $this;
	}
	function rightJoin($str){
		$this->right_join[] = $str;
		return $this;
	}
	function where($str){
		$this->wheres[] = $str;
		return $this;
	}
	function whereOr( $str, $group ){
		$this->whereors[$group][] = $str;
		return $this;
	}
	function order($str){
		$this->orders[] = $str;
		return $this;
	}
	function group($str){
		$this->groups[] = $str;
		return $this;
	}
	function limit($limit_offset){
		$aux = explode(',', $limit_offset);
		$this->limits = array($aux[0], @$aux[1]);
		return $this;
	}
	function having($str){
		$this->having[] = $str;
		return $this;
	}
	function query($show = false, $string = false){
		foreach( $this->whereors as $group=>$wheres){
			$this->wheres[] = '( ' .implode(" or ", $wheres) .' )';
		}

		if( !$this->selects )$this->selects = array('*');

		$sql_arr = array(
			' select '.implode(',',$this->selects),
			' from '.implode(',', $this->froms) ,
			implode(' ',array_map('left_join',$this->left_join) ),
			implode(' ',array_map('right_join',$this->right_join) ),
			$this->wheres ? 'where '.implode(' and ', $this->wheres) : '',
			$this->groups ? 'group by '.implode(',', $this->groups) : '',
			$this->having ? 'having '.implode(' and ', $this->having) : '',
			$this->orders ? 'order by '.implode(',', $this->orders) : '',
			@$this->limits[0] ? ' limit '.$this->limits[0] : '',
			@$this->limits[1] ? ' offset '.$this->limits[1] : ''
		);
		$query = implode(' ', $sql_arr);
		if($show) Console( $query );
		if($string) return $query;
		return sqlQuery( $query );
	}
	function queryString( $show = false ){
		return $this->query($show, true);
	}
}
function sqlBuild(){
	return new SqlBuild_();
}

?>